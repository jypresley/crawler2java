import gui.ava.html.image.generator.HtmlImageGenerator;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mysql.jdbc.StringUtils;


import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;

import javax.imageio.ImageIO;


import net.htmlparser.jericho.*;


public class CheckRobotsTxt {

	final static int constProcessReadRobotsTxt = 0; //0 or 1

	final static int constCleanedTablesCrawling = 0; //0 or 1 DELETE FROM CRAWLEDSITES & CRAWLEDSITES_URL

	final static int constProcessCrawlingSites = 0; //0 or 1 DATA FROM CRAWLEDSITES_URL, with depth=depth++
	final static int constProcessCrawlingSitesDepth1 = 1; //0 or 1

	final static int constProcessCrawlingSites_CaptureURLs = 1; //0 or 1 URLS capture feed => CRAWLEDSITES_URL
	final static int constProcessCrawlingSites_CaptureURLsDepthPermitted = 1; //0=only depth 0 store, 1=depth 0&1 store, etc.
	
	
	final static int constProcessSTARTlevelCrawling = 1; //0=start, 1=dept1, 2=depth2, etc..
	
	
	final static int constProcessOverwriteWhenCrawled = 0; //0=skip crawledsites, 1=delete then recapture
	
	
    //"select top 1 Url from SITES where Url like '%bluebayproperties.mu%'";
    //"select Url from SITES where url like '%banquedesmascareignes.mu%'";
   //"select Url from SITES";
	//final static String constSITEQUERY = "select top 1000 Url from SITES --where url like '%http://ministry-education.gov.mu%' or url like '%http://androidmauritius.com%'";
	
	//final static String constSITEQUERY = "select top 1000 Url from SITES where url like '%www.konetou.mu%'";
	
	//depth 0
//	final static String constSITEQUERY = "select top 1000 Url,'' from SITES where url like '%www.konetou.mu%' -- or url like '%portailmaurice.astek.mu%'";
	
	//depth 1
//	final static String constSITEQUERYDepth1 = "select top 1000 LINK,fk_crawledsites_crawled_url from CRAWLEDSITES_LINKS where depth=0 and LINK  like '%http://www.konetou.mu%' -- or url like '%portailmaurice.astek.mu%'";
	
	
	
	/*
	//depth 0
	final static String constSITEQUERY = "select top 10000 Url,'' from SITES where enable=-1 --where url like '%www.konetou.mu%' -- or url like '%portailmaurice.astek.mu%'";
	
	//depth 1
	final static String constSITEQUERYDepth1 = "select distinct top 100000  LINK,fk_crawledsites_crawled_url from CRAWLEDSITES_LINKS,SITES where CRAWLEDSITES_LINKS.fk_crawledsites_crawled_url like '%' + SITES.BasicDomain + '%' and CRAWLEDSITES_LINKS.depth=1 and LINK  like 'http://%' and Right(LINK,4) <> '.JPG'-- or url like '%portailmaurice.astek.mu%'";
*/
	
	//depth 0
	//final static String constSITEQUERY = "select  distinct LINK as Url,'' from CRAWLEDSITES_LINKS where CRAWLEDSITES_LINKS.depth=1 and processed=0 and LINK  like 'http://%' and (Right(LINK,4) <> '.JPG' and Right(LINK,4) <> '.GIF' and Right(LINK,4) <> '.PDF' and Right(LINK,4) <> '.ZIP' and Right(LINK,4) <> '.TAR' and Right(LINK,4) <> '.AVI' and Right(LINK,4) <> '.EXE' and Right(LINK,4) <> '.BIN' and Right(LINK,4) <> '.JAR' and Right(LINK,4) <> '.MSI' and Right(LINK,3) <> '.GZ' and Right(LINK,4) <> '.DMG') and LINK  like '%.mu%' and LINK not like 'http://ads.%'-- or url like '%portailmaurice.astek.mu%'";
	
	//final static String constSITEQUERY = "select  distinct LINK as Url,'' from CRAWLEDSITES_LINKS where CRAWLEDSITES_LINKS.depth=1 and processed=0 and LINK  like 'http://%' and (Right(LINK,4) <> '.JPG' and Right(LINK,4) <> '.GIF' and Right(LINK,4) <> '.PDF' and Right(LINK,4) <> '.ZIP' and Right(LINK,4) <> '.TAR' and Right(LINK,4) <> '.AVI' and Right(LINK,4) <> '.EXE' and Right(LINK,4) <> '.BIN' and Right(LINK,4) <> '.JAR' and Right(LINK,4) <> '.MSI' and Right(LINK,3) <> '.GZ' and Right(LINK,4) <> '.DMG') and LINK  like '%.mu%' and LINK not like 'http://ads.%'-- or url like '%portailmaurice.astek.mu%'";
	final static String constSITEQUERY = "select  distinct LINK as Url,'' from CRAWLEDSITES_LINKS inner join [DOMAINS] on [CRAWLEDSITES_LINKS].ROOTDOMAIN = [DOMAINS].rooturl where [DOMAINS].EXCLUSION = 0 and [CRAWLEDSITES_LINKS].processed =0 and [CRAWLEDSITES_LINKS].valid=1 order by Url";
	
	//depth 1
	final static String constSITEQUERYDepth1 = "qsds";

	

	/*
	SELECT     id, fk_crawledsites_id, fk_crawledsites_crawled_url, LINKLABEL, LINK, SAMESITE, DEPTH, MASTERDOMAIN, datecrea
	FROM         CRAWLEDSITES_LINKS
	*/
	
	final static int constThreadReadRobotsTxt = 90000;
	final static int constThreadCrawlSite = 900000;
	
	
	
	
	static Boolean check = false;
	static String returncode = "";
	static int err = 0;
	
	static String[] hasrobots = new String[1];
	static String[] body = new String[1];
	
	static String[] ContentCharSet = new String[1];
	static String[] ContentMimeType = new String[1];
	
	static String[] dnsResolving = new String[1];
	
	static String[] depth = new String[1];
	
	
	
	
    static DbConn dbc = new DbConn();
    static DbConn dbc2 = new DbConn();
    static Date date = new Date();
    static SimpleDateFormat sdf;
    String ex =null;
    
    
    Vector<String> basicDomains = new Vector<String>();
    
    
    
    static Vector<String> sitesdepth1a; //depth 1
    
    //static String masterurl = "";
    
    
    
    
//	public void insertMailsFalse(emailsFalse){
//		
//	}
    
    
    /*void method() {
    	long endTimeMillis = System.currentTimeMillis() + 10000;
    	while (true) {
    		// method logic
    		if (System.currentTimeMillis() > endTimeMillis) {
    			// do some clean-up
    			return;
    		}
    	}
    }*/
    
    
    
    
    /*
	public void insertMails(String emailTrue, long elapsedTime, String checkEmail){
		
		//ResultSet rs = null;
	    Connection conn = null;
	    Statement stmt = null;
	    
		try {
		      conn = dbc.getConnection();
		    	
		      stmt = conn.createStatement();
		      
		      
		      //String query1 = "update dbo.SAMPLENEW WITH (tablock) set processed = 1, reprocess = reprocess + 1, dateprocessed=getdate() where txt = '" + emailTrue + "' ";
		      
		      //String query1 = "update dbo.SAMPLENEW WITH (tablock) set processed = 1, dateprocessed=getdate() where txt = '" + emailTrue + "' ";
		      String query1 = "update dbo.SAMPLENEW WITH (tablock) set processed = 1, stamp=123, dateprocessed=getdate() where txt = '" + emailTrue + "' ";
		      //String query1 = "update dbo.SAMPLENEW WITH (tablock) set processed = 1, stamp=234, dateprocessed=getdate() where txt = '" + emailTrue + "' ";
		      //String query1 = "update dbo.SAMPLENEW WITH (tablock) set processed = 1, stamp=500, dateprocessed=getdate() where txt = '" + emailTrue + "' ";
		      //String query1 = "update dbo.SAMPLENEW WITH (tablock) set processed = 1, stamp=601, dateprocessed=getdate() where txt = '" + emailTrue + "' ";
		      //String query1 = "update dbo.SAMPLENEW WITH (tablock) set processed = 1, stamp=699, dateprocessed=getdate() where txt = '" + emailTrue + "' ";
		      
		      //finish misc
		      //String query1 = "update dbo.SAMPLENEW WITH (tablock) set processed = 1, stamp2=888, dateprocessed=getdate() where txt = '" + emailTrue + "' ";

		      
		      System.out.println(query1);
		      
		      stmt.executeUpdate(query1);
		      
		  if (checkEmail == "OK"){
		      
			  String query2 = "update dbo.SAMPLENEW WITH (tablock) set processed = 2 where txt = '" + emailTrue + "' ";
			  System.out.println(query2);
			  stmt.executeUpdate(query2);
		      
		     String query = "insert into dbo.PROCESSED_OK values ('" +emailTrue+"', getdate(), '"+elapsedTime+"') ";
		     System.out.println(query);
		     stmt.executeUpdate(query);
  		      
		     }
		      else {
		    	  if (elapsedTime > 60000){
		    		 ex= " > 60000 ms" ;
		    	  }
		    	  else{
		    		  //ex = "timeout";
		    		  ex = checkEmail;
		    	  }
		    	 String query = "insert into dbo.PROCESSED_KO values ('" +emailTrue+"', getdate(), '"+elapsedTime+"', left('"+ex+"',255)) ";
		    	 System.out.println(query); 
		    	 stmt.execute(query);
		      }
		
		} catch (Exception e) {
		      e.printStackTrace();
		    } finally {
		      try {
		        //rs.close();
		        stmt.close();
		        conn.close();
		      } catch (SQLException e) {
		        e.printStackTrace();
		      }
		    }
}
	*/
    
	
    
    //tdb1.updateSites(vtr, elapsedTimeMillis, returncode, hasrobots[0], body[0],ContentCharSet[0], ContentMimeType[0]);
    
public void updateSites(String url, long elapsedTime, String returncode, String hasrobots, String body, String ContentCharSet, String ContentMimeType, String dnsResolving){
		
		//ResultSet rs = null;
	    Connection conn = null;
	    Statement stmt = null;
	    
	    String query1 = "";
	    
		try {
		      conn = dbc.getConnection();
		    	
		      stmt = conn.createStatement();
		      
		      
		      //query1 = "update SITES set TestRobotTimetaken=" + elapsedTime + ",TestRobotReturnCode='"+returncode+"',HasRobots="+hasrobots+",RobotsTxt='" + ParseSQL(body) + "', RobotsContentMimeType = '" + ContentMimeType + "',RobotsContentCharSet='" + ContentCharSet + "',dnsResolving='" + dnsResolving + "',LastCrawled=getdate() where Url = '" + url + "'";
		      query1 = "update SITES set TestRobotTimetaken=" + elapsedTime + ",TestRobotReturnCode='"+returncode+"',HasRobots="+hasrobots+",RobotsTxt='" + ParseSQL(body) + "', RobotsContentMimeType = '" + ContentMimeType + "',RobotsContentCharSet='" + ContentCharSet + "',dnsResolving='" + dnsResolving + "',LastCrawled=getdate() where Url = '" + url + "'";

		      //System.out.println(query1);
		      
		      stmt.executeUpdate(query1);
		      
		 
		} catch (Exception e) {
		    
			//System.out.println(query1);
			e.printStackTrace();
		      
		      
		    } finally {
		      try {
		        //rs.close();
		        stmt.close();
		        conn.close();
		      } catch (SQLException e) {
		        e.printStackTrace();
		      }
		    }
}
    
  
public static void CleanedTablesCrawling() {
	
    Connection conn = null;
    Statement stmt = null;
    
    String query0 = "";
    String query1 = "";
    
    
	try {
	      conn = dbc.getConnection();
	    	
	      stmt = conn.createStatement();
	      
	      query0 = "delete from CRAWLEDSITES";
	      query1 = "delete from CRAWLEDSITES_LINKS";
	      
	      //System.out.println(query1);
	      stmt.executeUpdate(query0);
	      stmt.executeUpdate(query1);
	      
	      
	 
	} catch (Exception e) {
	    
		//System.out.println(query1);
		e.printStackTrace();
	      
	      
	    } finally {
	      try {
	        //rs.close();
	        stmt.close();
	        conn.close();
	      } catch (SQLException e) {
	        e.printStackTrace();
	      }
	    }
	    
	
}



public static String SearchDataInTableReturnsOneString(String DefnTable, String DefnField, String DefnSearchCriteria, String DefnFieldToReturn) {
	
	
	ResultSet rs = null;
    Connection conn = null;
    Statement stmt = null;
    
    conn = dbc.getConnection();
    try {
		stmt = conn.createStatement();
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
        
    String query = "select "+DefnFieldToReturn+" from "+DefnTable+" where "+DefnField+" = '"+DefnSearchCriteria+"'";
   // System.out.println("querySearchDataInTableReturnsOneString="+query);
    
      String mystring = "";
	    try {
		      
	      rs = stmt.executeQuery(query);
	      while (rs.next()) {
	    	mystring+=rs.getString(1) ;
	      }
	      
	    } catch (Exception e) {
	      e.printStackTrace();
	    } finally {
	      try {
	        rs.close();
	        stmt.close();
	        conn.close();
	    	  
	      } catch (SQLException e) {
	        e.printStackTrace();
	      }
	    
	    }
	    
	 	return mystring;
	
}




public static boolean SearchDataInTable(String DefnTable, String DefnField, String DefnSearchCriteria) {
	
	
	ResultSet rs = null;
    Connection conn2 = null;
    Statement stmt2 = null;
    
    
    
    try {
    	conn2 = dbc2.getConnection();
		stmt2 = conn2.createStatement();
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
        
    String query = "select "+DefnField+" from "+DefnTable+" where "+DefnField+" = '"+DefnSearchCriteria+"'";
      boolean toreturn = false;
      
	    try {
		      
	      rs = stmt2.executeQuery(query);
	      while (rs.next()) {
	    	  toreturn = true;
	    	  break;
	      }
	    } catch (Exception e) {
	      e.printStackTrace();
	    } finally {
	      try {
	        rs.close();
	        stmt2.close();
	        conn2.close();
	    	  
	      } catch (SQLException e) {
	        e.printStackTrace();
	      }
	    
	    }
	    
	 	return toreturn;
	
}


public static String URLSimpleTrim(String theurl) {
	String theupdatedurl = theurl;
	
	try {
		if (theurl.endsWith("/")) {
			theupdatedurl = theurl.substring(0, theurl.length()-1);
		}	
	} catch (Exception e) {
		
	}
	
	
	return theupdatedurl;
}


public void updateCrawledSites(String mainurl, String crawlurl, long elapsedTime, String returncode, String depth, String body, String ContentCharSet, String ContentMimeType, String dnsResolving){
	
	
    Connection conn = null;
    Statement stmt = null;
    
    String query00 = "";
    String query0 = "";
    String query1 = "";
    String query2 = "";
    
    String depthPlus = depth;
    depthPlus = String.valueOf((Integer.parseInt(depth)+1)).toString();
    
    
	try {
	      conn = dbc.getConnection();
	    	
	      stmt = conn.createStatement();
	      
	      if (constProcessOverwriteWhenCrawled == 1) { 
	    	  query0 = "delete from CRAWLEDSITES where crawled_url = '" + crawlurl + "'";
	      }
	     
	      
	      
	      
	      
	      //check W3C
	      String seocompliant = "0";
	      /*W3cMarkupValidator w3 = new W3cMarkupValidator();
	      if (w3.validate(body).isValid()) {
	    	  seocompliant = "1";
	      } else
	      {
	    	  seocompliant = "0";
	      }*/
	      
	      
	      String optimisedBodyParsed = ParseSQL(ScriptCleanBody(body)) + " " + ParseSQL(ExtractMetakeywords(body)) + " " + ParseSQL(ExtractMetadescription(body));
	      
	      String TheuserCredibilityRank = "0";
	      TheuserCredibilityRank =  CheckRobotsTxt.SearchDataInTableReturnsOneString("SITES", "Url", crawlurl,"UserCredibilitiesRank");
	      
	      try {
	    	  int triedint = Integer.parseInt(TheuserCredibilityRank);	
	      } catch (Exception e) {
	    	  TheuserCredibilityRank = "0";
	      }
	     
	      

	      //pre-prepare get all links
	        Source source=new Source(new String(body));
	        	//	      System.out.println("\nLinks to other documents:");
			  	List<Element> linkElements=source.getAllElements(HTMLElementName.A);

			  	
			  	
			  	//preprocessing amt of links
			  	int AmountOfGenuineLinks = 0;
			  	for (Element linkElement : linkElements) {
			  		String href=linkElement.getAttributeValue("href");
			  		if (href==null) continue;
			  		if (isGenuineLink(href)) {
			  			AmountOfGenuineLinks+=1;
			  		}
			    	
			  	}

			  	System.out.println("ContentCharSet=" + ContentCharSet);
			  	System.out.println("ContentMimeType=" + ContentMimeType);
			  	
			  	//generate image
			  	HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
			  	imageGenerator.loadHtml(body);
			  	String base64sitepreview = "";
			  	try {
			  					  		
			  		//application/msword
			  		if (ContentMimeType.equals("text/html")) {

			  			try {
			  				BufferedImage bi = imageGenerator.getBufferedImage();
			  				ByteArrayOutputStream os = new ByteArrayOutputStream();

						  	OutputStream b64 = new Base64.OutputStream(os);

						  	ImageIO.write(bi, "png", b64);
						  	base64sitepreview = os.toString("UTF-8");
						  	bi.flush();
			  			} catch (OutOfMemoryError oe) {
			  				oe.printStackTrace();
			  			}
				  		
					  	
					  	runGarbageCollector();
			  		}
			  		
				  	
			  	} catch (Exception e) {
			  		e.printStackTrace();
			  	}
			  	
			  	
			  	
			  	
			  	//base64sitepreview = "test";
			  	
			  	

			  				  	
	          
	      //query1 = "update CRAWLEDSITES set TestRobotTimetaken=" + elapsedTime + ",TestRobotReturnCode='"+returncode+"',HasRobots="+hasrobots+",RobotsTxt='" + ParseSQL(body) + "', RobotsContentMimeType = '" + ContentMimeType + "',RobotsContentCharSet='" + ContentCharSet + "',dnsResolving='" + dnsResolving + "',LastCrawled=getdate() where Url = '" + url + "'";
	      query1 = "insert into CRAWLEDSITES (fk_sites_url,crawled_url,returncode,dnsResolving,pagetitle,metakeywords,metadescription,contentmimetype,contentcharset,depth,sitepreview,contents_original,contents_scriptcleaned,contents_optimised,seocompliant,nbLinksOnPage,userCredibilityRank, datecrawled,timetaken_ms)";
	      query1 = query1 + " values ('"+ mainurl + "','"+ crawlurl + "','" + returncode + "','"+ dnsResolving + "','"+ ParseSQL(ExtractPageTitle(body))+"','"+ ParseSQL(ExtractMetakeywords(body))+ "','"+ParseSQL(ExtractMetadescription(body))+ "','"+ContentCharSet+"','"+ContentMimeType+"','"+depth+ "','" + ParseSQL(base64sitepreview) +  "','"+ParseSQL(body)+"','"+ParseSQL(ScriptCleanBody(body))+"','" + optimisedBodyParsed + "','" + seocompliant + "','"+String.valueOf(AmountOfGenuineLinks)+"','"+TheuserCredibilityRank+"',getdate(),"+elapsedTime+")";

	    // boolean test = searchDataInTable('' , 'contents_optimised', optimisedBodyParsed);
	      
	     // System.out.println("query1="+query1);
	      if (constProcessOverwriteWhenCrawled == 1) { 
	    	  stmt.executeUpdate(query0);
	      }
	      
	      if (!SearchDataInTable("CRAWLEDSITES", "crawled_url", URLSimpleTrim(crawlurl))) {
	    	  stmt.executeUpdate(query1);
	    	  System.out.println("inserted OK:"+crawlurl);
	      } else {
	    	  System.out.println("already exists (not inserted):"+crawlurl);
	      }
		  
		      
		      
		      
		      //insert ALL links in table
				  		

				  	
				if (constProcessCrawlingSites_CaptureURLs == 1 && constProcessCrawlingSites_CaptureURLsDepthPermitted >= Integer.parseInt(depth)) {
					int j=0;
				  	for (Element linkElement : linkElements) {
				  		String href=linkElement.getAttributeValue("href");
				  		if (href==null) continue;
				  		j=j+1;
				  		// A element can contain other tags so need to extract the text from it:
				  		String label=linkElement.getContent().getTextExtractor().toString();
				  		//System.out.println(label+" <"+href+'>');
				  		query2 = "insert into CRAWLEDSITES_LINKS (fk_crawledsites_id,fk_crawledsites_crawled_url,LINKLABEL,LINK,SAMESITE,DEPTH,MASTERDOMAIN)";
				  		query2 = query2 + "values (0,'"+ParseSQL(crawlurl)+"','"+ParseSQL(label)+"','"+ParseSQL(href)+"',0,'"+depthPlus+"','"+ParseSQL(ExtractMasterDomain(href))+"')";
				  		
				        stmt.executeUpdate(query2);
				        //System.out.println("capture_url_query="+query2);				  			
				        
				  	}
				  	
				  	System.out.println("amount of captured url="+j);
				  	
				  	//UpdateBDD(String DefnTABLE, String DefnFIELDTOUPDATE, String DefnFIELDVALUE, String DefnFIELDCONDITION, String DefnFIELDCONDITIONVALUE  )
				  	//UpdateBDD("CRAWLEDSITES","nbLinksOnPage",String.valueOf(AmountOfGenuineLinks),"crawled_url",ParseSQL(crawlurl));
				}
	 
	} catch (Exception e) {
	    
		//System.out.println(query1);
		e.printStackTrace();
	      
	      
	    } finally {
	      try {
	        //rs.close();
	        stmt.close();
	        conn.close();
	      } catch (SQLException e) {
	        e.printStackTrace();
	      }
	    }
	    
	    
	    
	    //System.out.println("RenderToText=" + RenderToText(body));
}


public static void UpdateBDD(String DefnTABLE, String DefnFIELDTOUPDATE, String DefnFIELDVALUE, String DefnFIELDCONDITION, String DefnFIELDCONDITIONVALUE  ){
	
    Connection conn = null;
    Statement stmt = null;
    
    String query1 = "";
    
	try {
	      conn = dbc.getConnection();
	      stmt = conn.createStatement();
	      
	      query1 = "update "+DefnTABLE+" set "+DefnFIELDTOUPDATE+"='" + DefnFIELDVALUE + "' where "+DefnFIELDCONDITION+" = '" + DefnFIELDCONDITIONVALUE + "'";

	      //System.out.println(query1);
	      
	      stmt.executeUpdate(query1);
	      
	 
	} catch (Exception e) {
	    
		//System.out.println(query1);
		e.printStackTrace();
	      
	      
	    } finally {
	      try {
	        //rs.close();
	        stmt.close();
	        conn.close();
	      } catch (SQLException e) {
	        e.printStackTrace();
	      }
	    }
}



public static boolean isGenuineLink(String thelink) {
	if (thelink.indexOf("http://") == 0) {
		//System.out.println("thelink="+thelink);
		return true;
	}
	else {
		return false;
	}
}


public String ExtractMasterDomain(String url) {
	String basicurl = basicDomain(url);
	String masterdomain = "";
	
	if (basicurl.length() == 0) {
		return "N/A";
	}
	
	//System.out.println("basicurl=" + basicurl);
	//System.out.println("basicurllength=" + basicurl.length());
	
	for (int i = basicurl.length()-1; i > 0; i--) {  
		if (basicurl.charAt(i) == '.') {
			break;
		}
	    masterdomain+=basicurl.charAt(i);
	}
	return masterdomain;
}



public String RenderToText(String body) {
	
	Source source=new Source(new String(body));
	//String renderedText=source.getRenderer().toString();
	//System.out.println("\nSimple rendering of the HTML document:\n");
	//System.out.println(renderedText);
	
	//return source.getTextExtractor().setIncludeAttributes(true).toString();
	return source.getTextExtractor().setIncludeAttributes(false).toString();


	//return renderedText;
}


public String ExtractPageTitle(String contents) {
	//return contents;
	//return "test";
	
	String title = "";
	
	/*Pattern p = Pattern.compile("<head>.*?<title>(.*?)</title>.*?</head>", Pattern.DOTALL); 
	Matcher m = p.matcher(contents);
	while (m.find()) {
	    title = m.group(1);
	}*/
	
	
	Source source=new Source(new String(contents));

	// Call fullSequentialParse manually as most of the source will be parsed.
	source.fullSequentialParse();
	Element titleElement=source.getFirstElement(HTMLElementName.TITLE);
	if (titleElement==null) return null;
	// TITLE element never contains other tags so just decode it collapsing whitespace:
	return CharacterReference.decodeCollapseWhiteSpace(titleElement.getContent());
	
}

public String ExtractMetakeywords(String contents) {
	//return contents;
	
	Source source=new Source(new String(contents));
	
	
	for (int pos=0; pos<source.length();) {
		StartTag startTag=source.getNextStartTag(pos,"name","keywords",false);
		if (startTag==null) return null;
		if (startTag.getName()==HTMLElementName.META)
			return startTag.getAttributeValue("content"); // Attribute values are automatically decoded
		pos=startTag.getEnd();
	}
	return null;
	
}
    
public String ExtractMetadescription(String contents) {
	//return contents;
	
	Source source=new Source(new String(contents));
	
	
	for (int pos=0; pos<source.length();) {
		StartTag startTag=source.getNextStartTag(pos,"name","description",false);
		if (startTag==null) return null;
		if (startTag.getName()==HTMLElementName.META)
			return startTag.getAttributeValue("content"); // Attribute values are automatically decoded
		pos=startTag.getEnd();
	}
	return null;
	
}

public String ScriptCleanBody(String contents) {
	

    
	//return removeTag(removeHTML(removeTagRegx1(removeTagRegx2(contents))));
	return RenderToText(contents);
	//return removeTagRegx1(contents);
	//return "test";
}




public String removeTag(String data){
	StringBuilder regex = new StringBuilder("<script[^>]*>(.*?)</script>");
	int flags = Pattern.MULTILINE | Pattern.DOTALL| Pattern.CASE_INSENSITIVE;
	Pattern pattern = Pattern.compile(regex.toString(), flags);
	Matcher matcher = pattern.matcher(data);
	return matcher.replaceAll("");
}

public static String removeHTML(String htmlString)
{
      // Remove HTML tag from java String    
    String noHTMLString = htmlString.replaceAll("\\<.*?\\>", "");

    // Remove Carriage return from java String
    noHTMLString = noHTMLString.replaceAll("\r", "<br/>");

    // Remove New line from java string and replace html break
    noHTMLString = noHTMLString.replaceAll("\n", " ");
    noHTMLString = noHTMLString.replaceAll("\'", "&#39;");
    noHTMLString = noHTMLString.replaceAll("\"", "&quot;");
    return noHTMLString;
}

public static String removeTagRegx1(String htmlString)
{
     return htmlString.replaceAll("\\<.*?\\>", "");
}

public static String removeTagRegx2(String htmlString)
{
     return htmlString.replaceAll("<(.*?)*>", "");
}



public void updateBasicDomain(String url, String basicdomain){
	
	//ResultSet rs = null;
    Connection conn2 = null;
    Statement stmt = null;
    String query1 = "";
    
	try {
	      conn2 = dbc2.getConnection();
	    	
	      stmt = conn2.createStatement();
	      
	      
	      query1 = "update SITES set basicDomain = '" + ParseSQL(basicdomain) + "' where Url = '" + ParseSQL(url) + "'";

	      //System.out.println(query1);
	      
	      stmt.executeUpdate(query1);
	      
	 
	} catch (Exception e) {
		System.out.println(query1);
		
	      e.printStackTrace();
	    } finally {
	      try {
	        //rs.close();
	        stmt.close();
	        conn2.close();
	      } catch (SQLException e) {
	        e.printStackTrace();
	      }
	    }
}
    

    
    
    
    
    
	
	
	
	/*
	public Vector<String> getEmails(){
		
		ResultSet rs = null;
	    Connection conn = null;
	    Statement stmt = null;
        
        String emailAdd = null;
        Vector<String> em = new Vector<String>();
        
	    
		   
		    try {
		      conn = dbc.getConnection();
		    	
		      stmt = conn.createStatement();
		      //String query = "select top 500000 * from dbo.SAMPLENEW s where processed = 1 --and reprocess = 0 --WITH (nolock) order by NEWID();";
		      
		      String query = "select * from SAMPLENEW, PROCESSED_KO where SAMPLENEW.txt =  PROCESSED_KO.txt and left(exception,2) = ' >' and stamp <> 123";
		      
		      
		      //String query = "select * from SAMPLENEW, PROCESSED_KO where SAMPLENEW.txt =  PROCESSED_KO.txt and exception = 'NOT OK' and (stamp <> 234 or stamp is null)";
		      
		      //String query = "select * from SAMPLENEW, PROCESSED_KO where SAMPLENEW.txt =  PROCESSED_KO.txt and exception like 'Sender rejected%' and (stamp <> 500 or stamp is null)";
		      
		      
		      //String query = "select * from SAMPLENEW, PROCESSED_KO where SAMPLENEW.txt =  PROCESSED_KO.txt and exception like 'Thread timeout%'  and (stamp <> 601 or stamp is null)";

		      //String query = "select * from SAMPLENEW, PROCESSED_KO where SAMPLENEW.txt =  PROCESSED_KO.txt and exception like 'EXCEPTION: java%' and (stamp is null or stamp <> 699)";

		      
		     // String query = "select * from SAMPLENEW, PROCESSED_KO where SAMPLENEW.txt =  PROCESSED_KO.txt and processed <> 2 and (exception like 'EXCEPTION: java%')";
		      
		      
		
		      
		      
		      rs = stmt.executeQuery(query);
		      
		      while (rs.next()) {
		    	
		        emailAdd = rs.getString(1);
		        em.add(emailAdd);
              
		      }
		    } catch (Exception e) {
		      e.printStackTrace();
		    } finally {
		      try {
		        rs.close();
		        stmt.close();
		        conn.close();
		    	  
		      } catch (SQLException e) {
		        e.printStackTrace();
		      }
		    
		    }
			return em;
	      }
	*/
	
	
	
    
     
	
	
	
	
public Vector<String> getSites(int depth) throws SQLException{
		
	String thebasicDomain = "";
	
		ResultSet rs = null;
	    Connection conn = null;
	    Statement stmt = null;
        
        String siteAdd = null;
        String siteAdd2 = null;
        Vector<String> em = new Vector<String>();
        Vector<String> em2 = new Vector<String>();
        
        conn = dbc.getConnection();

	      stmt = conn.createStatement();
	        
	     // String query = "select top 1 Url from SITES where Url like '%bluebayproperties.mu%'";
	      //String query = "select Url from SITES where url like '%banquedesmascareignes.mu%'";
	     // String query = "select Url from SITES";
	      
	      String query = "";
	      
	      if (depth == 0) {
	    	  query = constSITEQUERY;
	      }
	      
	      if (depth == 1) {
	    	  query = constSITEQUERYDepth1;
	      }
	      
	      
	      //BYPASS
	      query = constSITEQUERY;
	      depth = 0;
	      
	      
	      System.out.println(query.toString());
	     // System.exit(0);
		   
		    try {

			      
		      rs = stmt.executeQuery(query);
		      
		      while (rs.next()) {
		    	
		    	  siteAdd = rs.getString(1); //actual data
		    	  siteAdd2 = rs.getString(2); //previous site
		    	  
		    	  thebasicDomain = basicDomain(siteAdd); //calculate
		    	  basicDomains.add(thebasicDomain); //add to vector
		    	  //updateBasicDomain(siteAdd,thebasicDomain); //update BDD //*********---------
		    	  
		          em.add(siteAdd); //actual data
		          em2.add(siteAdd2);  //previous site
              
		      }
		    } catch (Exception e) {
		      e.printStackTrace();
		    } finally {
		      try {
		        rs.close();
		        stmt.close();
		        conn.close();
		    	  
		      } catch (SQLException e) {
		        e.printStackTrace();
		      }
		    
		    }
		    
		    if (depth == 1) {
		    	 sitesdepth1a = em2; //previous site catch
		      }
		   
		    
			return em;
	}
	
	
public static String basicDomain(String url) {
	String basicUrl = "";
	
		
	try{
		//System.out.println("basicDomain url: "+url);

		  String urlAddress = url;
		  URL hosturl = new URL(urlAddress);
		  String domain = hosturl.getHost();
		  basicUrl =  domain;
		  
	}catch (Exception e){
	  //System.out.println("basicDomain: Exception caught ="+e.getMessage());
	  basicUrl = e.getMessage();
	  }

	
	return basicUrl;
}


public static String ParseSQL(String sql) {
	
	if (sql == null) {
		sql = "";
	}
	String constructedSQL = "";
	constructedSQL = sql.replace("'", "''");	
	
	return constructedSQL;
}







	@SuppressWarnings("deprecation")
	public  static void coreReadRobotsTxt(Vector<String> sites, int index, int maxthreads) {
		
		
		hasrobots[0] = "0";
		body[0] = "";
		ContentCharSet[0] = "";
		ContentMimeType[0] = "";
		
		
		CheckRobotsTxt tdb1 = new CheckRobotsTxt();
		
		
		     //for (int i = ((sites.size()/maxthreads)*index); i < (sites.size()/maxthreads)*(index+1); i++){
			//for (int i = ((sites.size()/maxthreads)*index); i < (sites.size()/maxthreads)*(index+1); i++){
				for (int i = index; i < (index+maxthreads); i++) {
	          
	        	 
	        	 final String vtr =  (String) sites.elementAt(i);
	        	 
	        	 System.out.println(" start " + ((sites.size()/maxthreads)*index)+1);
	        	 System.out.println(" upto " + (sites.size()/maxthreads)*(index+1));
	        	 
	        	 
	        	 long start = System.currentTimeMillis();
	        	 long elapsedTimeMillis = 0;
	        	 
	             System.out.println("Starting thread..");
	          
		          
	          Thread thread = new Thread(new Runnable() {
	      		@Override
	      		public void run() {
	      			//method();
	      			//check2 = NtseTest.validateEmail(vtr); 
	      			Ntse nt1 = new Ntse();
	      			returncode = nt1.readRobotsTxt(basicDomain(vtr), hasrobots, body, ContentCharSet, ContentMimeType, dnsResolving);
	      			//System.out.println((mails.size()/maxthreads)*index)+1));
	          
	      			
	      		}
	      	});
	      	thread.start();
	      	
	      	
	      	long endTimeMillis = System.currentTimeMillis() + constThreadReadRobotsTxt;
	      	while (thread.isAlive()) {
	      		if (System.currentTimeMillis() > endTimeMillis) {
	      			// set an error flag
	      			thread.stop();
	      			returncode = "Thread timeout " + constThreadReadRobotsTxt;
	      			break;
	      		}
	      		try {
	      			Thread.sleep(500);
	      		}
	      		catch (InterruptedException t) {}
	      	}
	      	


	      	System.out.println( vtr + " has robots.txt ? " + returncode );
	        elapsedTimeMillis = System.currentTimeMillis() - start;
	    	System.out.println("The reading took " + elapsedTimeMillis + " milliseconds");
	    	    
	      	//tdb1.insertMails(vtr, elapsedTimeMillis, check2);
	    	
	    	//System.out.println("body passed by ref = " + body[0]);
	    	tdb1.updateSites(vtr, elapsedTimeMillis, returncode, hasrobots[0], body[0],ContentCharSet[0], ContentMimeType[0], dnsResolving[0]);
	 	    	   
	      }
	         
		//System.out.println("start thread");
				
				runGarbageCollector();
		
		
	}
	
	
	
	
	@SuppressWarnings("deprecation")
	public  static void coreReadCrawlingSite(Vector<String> sites, int index, int maxthreads, int thedepth, String previoussite) {
		
		
		depth[0] = String.valueOf(thedepth);
		body[0] = "";
		ContentCharSet[0] = "";
		ContentMimeType[0] = "";
		
		
		CheckRobotsTxt tdb1 = new CheckRobotsTxt();
		
		
		     //for (int i = ((sites.size()/maxthreads)*index); i < (sites.size()/maxthreads)*(index+1); i++){
			//for (int i = ((sites.size()/maxthreads)*index); i < (sites.size()/maxthreads)*(index+1); i++){
		for (int i = index; i < (index+maxthreads); i++) {
	          
	        	 
	        	 final String vtr =  (String) sites.elementAt(i);
	        	 //masterurl = vtr;
	        	 
	        	 
	        	 
	        	 
	        	 if (constProcessOverwriteWhenCrawled == 0) { //check first if exists
	        		 
	        		//ResultSet rs = null;
	        		    Connection conn2 = null;
	        		    Statement stmt = null;
	        		    String query1 = "";
	        		    
	        			try {
	        			      conn2 = dbc2.getConnection();
	        			    	
	        			      stmt = conn2.createStatement();
	        			      
	        			} catch (Exception e) {
	        				e.printStackTrace();
	        			}
	   	    	  ResultSet rs = null;
	   	    	  
	   	    	  String query00 = "select crawled_url from CRAWLEDSITES where crawled_url = '" + vtr + "'";
	   	    	  boolean hasrows = false;
	   	    	  
	   	    	  try {
	   			      
	   	    	      rs = stmt.executeQuery(query00);
	   	    	      while (rs.next()) {
	   	    	    	hasrows = true;
	   	    	    	break;
	   	    	      }
	   	    	      
	   	    	    } catch (Exception e) {
	   	    	      e.printStackTrace();
	   	    	    } finally {
	   	    	      try {
	   	    	        rs.close();
	   	    	       // stmt.close();
	   	    	        //conn.close();
	   	    	    	  
	   	    	      } catch (SQLException e) {
	   	    	        e.printStackTrace();
	   	    	      }
	   	    	    
	   	    	    }
	   	    	    
	   	    	    if (hasrows) {
	   	    	    	System.out.println("ALREADY EXISTS : "+ vtr + " crawled skipped for this site");
	   	    	    	//CRAWLEDSITES_LINKS - update *processed to 1
	   	    	    	UpdateBDD("CRAWLEDSITES_LINKS", "processed", "2", "LINK", vtr);
	   	    	    	
	   	    	    	break; //GO OUT OF LOOP
	   	    	    }
	   	    	    
	   	    	    
	   	    	    
	   	      }
	        	 
	        	 
	        	 
	        	 
	        	 
	        	 
	        	 if (!isGenuineLink(vtr)) {
	        		 System.out.println("***SKIP NOT GENUINE LINK: " + vtr);
	        		 continue;
	        	 }
	        	 
	        	 boolean AlreadyExists = false;
	   	      
	        	 
	        	 //************---------------***************
		   	      /*if (CheckRobotsTxt.SearchDataInTable("CRAWLEDSITES", "crawled_url", vtr)) {
		   	    	  AlreadyExists = true;
		   	    	  System.out.println("SKIPPED "+vtr+" AlreadyExists(1st)=true");
		   	    	  continue;
		   	      }
		   	      
		   	      if (CheckRobotsTxt.SearchDataInTable("CRAWLEDSITES", "crawled_url", URLSimpleTrim(vtr))) {
		   	    	  AlreadyExists = true;
		   	    	 System.out.println("SKIPPED "+vtr+" AlreadyExists(2nd)=true");
		   	    	  continue;
		   	      }*/
		        	 
	        	 
	        	 
	        	 
	        	 
	        	 System.out.println(" start " + ((sites.size()/maxthreads)*index)+1);
	        	 System.out.println(" upto " + (sites.size()/maxthreads)*(index+1));
	        	 
	        	 
	        	 long start = System.currentTimeMillis();
	        	 long elapsedTimeMillis = 0;
	        	 
	             System.out.println("Starting crawl thread..");
	          
		          
	          Thread thread = new Thread(new Runnable() {
	      		@Override
	      		public void run() {
	      			//method();
	      			//check2 = NtseTest.validateEmail(vtr);
	      			Ntse nt1 = new Ntse();
	      			returncode = nt1.readReadCrawlingSite(vtr, depth, body, ContentCharSet, ContentMimeType, dnsResolving);
	      			//System.out.println((mails.size()/maxthreads)*index)+1));
	          
	      			
	      		}
	      	});
	      	thread.start();
	      	
	      	
	      	long endTimeMillis = System.currentTimeMillis() + constThreadCrawlSite;
	      	while (thread.isAlive()) {
	      		if (System.currentTimeMillis() > endTimeMillis) {
	      			// set an error flag
	      			thread.stop();
	      			returncode = "Thread timeout " + constThreadCrawlSite;
	      			break;
	      		}
	      		try {
	      			Thread.sleep(500);
	      		}
	      		catch (InterruptedException t) {}
	      	}
	      	


	      	System.out.println( vtr + " = " + returncode );
	        elapsedTimeMillis = System.currentTimeMillis() - start;
	    	System.out.println("The reading crawl took " + elapsedTimeMillis + " milliseconds");
	    	    
	      	//tdb1.insertMails(vtr, elapsedTimeMillis, check2);
	    	
	    	//System.out.println("body passed by ref = " + body[0]);
	    	
	    	
	    	//public void updateCrawledSites(String mainurl, String crawlurl, long elapsedTime, String returncode, String depth, String body, String ContentCharSet, String ContentMimeType){
	    	//tdb1.updateCrawledSites(masterurl, vtr, elapsedTimeMillis, returncode, depth[0], body[0],ContentCharSet[0], ContentMimeType[0],dnsResolving[0]);
	    /*	if (ContentCharSet[0].equals("text/html")) {
	    			tdb1.updateCrawledSites(previoussite, vtr, elapsedTimeMillis, returncode, depth[0], body[0],ContentCharSet[0], ContentMimeType[0],dnsResolving[0]);		
	    	} else {
	    		System.out.println("NOT STORED : ContentCharSet[0]="+ContentCharSet[0]+" => " + vtr);
	    	}*/
	    	
	    	//application/xml
	    	//application/pdf
	    	//text/plain
	    	
	    	boolean skippedUpdate = false;
	    	if (!ContentMimeType[0].isEmpty()) {
	    		/*if (ContentMimeType[0].equals("text/plain") || ContentMimeType[0].equals("application/pdf") || ContentMimeType[0].equals("text/xml") || ContentMimeType[0].equals("application/xml") || ContentMimeType[0].equals("application/octet-stream")) {
	    			skippedUpdate = true;
	    		}*/
	    		if (ContentMimeType[0].equals("application/xml") || ContentMimeType[0].equals("application/octet-stream")) {
	    			skippedUpdate = true;
	    		}
	    	}

	    	if (!skippedUpdate) {
	    		tdb1.updateCrawledSites(previoussite, vtr, elapsedTimeMillis, returncode, depth[0], body[0],ContentCharSet[0], ContentMimeType[0],dnsResolving[0]);		
	    		
	    		//CRAWLEDSITES_LINKS - update *processed to 1
	    	    UpdateBDD("CRAWLEDSITES_LINKS", "processed", "1", "LINK", vtr);
    		
	    	} else {
	    		System.out.println("NOT STORED : ContentMimeType[0]="+ContentMimeType[0]+" => " + vtr);
	    		
	    		
	    		//CRAWLEDSITES_LINKS - update *processed to -1 *ERRORS
	    	    UpdateBDD("CRAWLEDSITES_LINKS", "processed", "-1", "LINK", vtr);
		    	
	
	    	}
    		 	    
	    	
		     
	      }
	         
		//System.out.println("start thread");
				
				 runGarbageCollector();
		
		
	}
	
	public static void runGarbageCollector() {
		// Step 1: get a Runtime object
	      Runtime r = Runtime.getRuntime();

	      //run garbage collector
	      r.gc();
	}
		

	
	public static void main(String[] args) throws Exception {
		
	      // Step 1: get a Runtime object
	      Runtime r = Runtime.getRuntime();
	      
		
	   // Step 2: determine the current amount of free memory
	      long freeMem = r.freeMemory();
	      System.out.println("free memory before running: " + freeMem);
	      long initialmemory = freeMem;
	      
	      
	      //run garbage collector
	      r.gc();
	      
		CheckRobotsTxt tdb = new CheckRobotsTxt();
		CheckRobotsTxt tdb2 = new CheckRobotsTxt();
		
		CheckRobotsTxt tdbDepth1 = new CheckRobotsTxt();
		final Vector<String> sites = tdb.getSites(0); //depth 0
		final Vector<String> sitesdepth1 = tdb.getSites(1); //depth 1
		
		int count = sites.size();
		int countdepth1 = sitesdepth1.size();
		
		System.out.println("Sites to processed (depth 0)= " + count + "\n");
		
		
		//CLEANING
		if (constCleanedTablesCrawling == 1) {
			System.out.println("Tables cleaned (crawledsites,crawledsites_links)");
			CleanedTablesCrawling();
		}
		
		
		//robots.txt
		if (constProcessReadRobotsTxt == 1) {
			for (int i=0; i < count; i++)
			{
				tdb.myReadRobotsTxtThread(sites,i);
				//System.out.println(i + ":" + (String) sites.elementAt(i) + "\n");
			}	
		}
		
		
		//read pages DEPTH 0
		if (constProcessSTARTlevelCrawling <= 0) {
			if (constProcessCrawlingSites == 1) {
				for (int i=0; i < count; i++)
				{
					System.out.println("myReadCrawlingSiteThread:depth0..");
					tdb.myReadCrawlingSiteThread(sites,i,0);
					//System.out.println(i + ":" + (String) sites.elementAt(i) + "\n");
					System.out.println("memory=" + r.freeMemory()+ " on " + initialmemory + " initially");
					
					  
				}
			}
		}
		
		
		
		//read pages DEPTH 1
		if (constProcessSTARTlevelCrawling <= 1) {
			if (constProcessCrawlingSitesDepth1 == 1) {
				
				System.out.println("Sites to processed (depth 1)= " + countdepth1 + "\n");
				
				for (int i=0; i < countdepth1; i++)
				{
					System.out.println("myReadCrawlingSiteThread:depth1..");
					tdbDepth1.myReadCrawlingSiteThread(sitesdepth1,i,1);
					//System.out.println(i + ":" + (String) sites.elementAt(i) + "\n");
					System.out.println("memory=" + r.freeMemory()+ " on " + initialmemory + " initially");

				}
			}
			
		}
		
		

		
		
		
	}
	
	
	
	public static void myReadRobotsTxtThread(final Vector<String> sites, final int i) {
		
		System.out.println("start ReadRobotsTxt thread " + i + " : " + sites.elementAt(i) + "\n");
		
		Thread thread1 = new Thread(new Runnable() {
	      	
			@Override
      		public void run() {
      			coreReadRobotsTxt(sites,i,1);
      		}
      	});
      	thread1.start();
      	
	}
	
	public static void myReadCrawlingSiteThread(final Vector<String> sites, final int i, int thedepth) {
		
		/*System.out.println("start ReadCrawlingSite thread " + i + " : " + sites.elementAt(i) + "\n");
		
		Thread thread1 = new Thread(new Runnable() {
	      	
			@Override
      		public void run() {
      			coreReadCrawlingSite(sites,i,1);
      		}
      	});
      	thread1.start();*/
		
		
			System.out.println("start ReadCrawlingSite NO-thread(function) " + i + " : " + sites.elementAt(i) + "\n");
		
			if (thedepth == 0) {
				coreReadCrawlingSite(sites,i,1,thedepth,sites.elementAt(i));
			}
			
			
			if (thedepth == 1) {
				//coreReadCrawlingSite(sites,i,1,thedepth,sitesdepth1a.elementAt(i));
				coreReadCrawlingSite(sites,i,1,thedepth,sites.elementAt(i));
			}
      		
      	
	}
	
	
	
	

}
